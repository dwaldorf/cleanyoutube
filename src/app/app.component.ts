import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent {
  title = 'scripttest';
  hasVideo = false;
  videoSrc = 'https://www.youtube.com/embed/QAwkGI-_adI?rel=0&enablejsapi=1';

  constructor(private activatedRoute: ActivatedRoute) {
    this.activatedRoute.queryParams.subscribe(params => {
      if (params.hasOwnProperty('v')) {
        let videoHash = params['v'];

        this.videoSrc = this.generateLink(videoHash);
        this.hasVideo = true;
      }
    });
  }

  public copyUrl(link: string) {
    let temp = this.generateLink(this.extractHash(link));

    const selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = temp;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);
  }

  public gotoUrl(link: string) {
    window.location.href = this.generateLink(this.extractHash(link));
  }

  private extractHash(link: string): string {
    return link.substring(link.lastIndexOf("?v=") + 3);
  }

  private generateLink(hash: string): string {
    return `https://www.youtube.com/embed/${hash}?rel=0&enablejsapi=1`;
  }
}
